package utils

import (
	"fmt"
	"log"
	"net/smtp"
)

func SendMailActivation(receiver string, userid string) error {
	// Choose auth method and set it up
	auth := smtp.PlainAuth("", "1b5f54a5b00ae5", "47cbbb39699b49", "smtp.mailtrap.io")

	// Here we do it all: connect to our server, set up a message and send it
	to := []string{receiver}
	server := "smtp.mailtrap.io:2525"
	senderMail := "From: <admin@cinco.co.id> \r\n"
	receiverMail := "To: <" + receiver + ">\r\n"
	subject := "Subject: Verify your account!\r\n"
	mime := "Content-Type: text/html; charset=\"utf-8\";\r\nContent-Transfer-Encoding: quoted-printable;\r\nContent-Disposition: inline;\r\n\r\n"
	body := "<html><body><center><h1>Click this link to activate your account</h1><p><a href=\"http://localhost/user/activation/" + userid + "\">Klik disini!</a></center></body></html>"
	msg := []byte(senderMail + receiverMail + subject + mime + body)
	err := smtp.SendMail(server, auth, "admin@cinco.co.id", to, msg)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(server, auth, to, string(msg))
	return err
}
