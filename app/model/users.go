package model

import (
	"time"
)

type User struct {
<<<<<<< HEAD
	Id         string    `gorm:"type:uuid;primary_key" json:"id"`
	Username   string    `json:"username"`
	Password   string    `json:"password"`
	Fullname   string    `json:"fullname"`
	Email      string    `json:"email"`
	BirthDate  time.Time `json:"birthdate"`
	Domicile   string    `json:"domicile"`
	Occupation string    `json:"occupation"`
	Status     bool      `json:"status"`
	gorm.Model
=======
	Id         string     `gorm:"type:uuid;primary_key" json:"id,omitempty"`
	Username   string     `json:"username,omitempty"`
	Password   string     `json:"password,omitempty"`
	Fullname   string     `json:"fullname,omitempty"`
	Email      string     `json:"email,omitempty"`
	BirthDate  time.Time  `json:"birthdate,omitempty"`
	Domicile   string     `json:"domicile,omitempty"`
	Occupation string     `json:"occupation,omitempty"`
	Status     bool       `json:"status,omitempty"`
	CreatedAt  time.Time  `json:"created_at,omitempty"`
	UpdatedAt  *time.Time `json:"updated_at,omitempty"`
>>>>>>> testCI
}
