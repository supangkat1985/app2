package handler

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/cinco/app/model"
)

type Handler struct {
	cashflowService interfaces.CashflowServiceInterface
}

type CincoCashflow interface {
	DoTransaction(ctx *fiber.Ctx) error
	CashflowEdit(c *fiber.Ctx) error
	CashflowDelete(c *fiber.Ctx) error
	CashflowHistory(c *fiber.Ctx) error
}

func (h Handler) DoTransaction(ctx *fiber.Ctx) error {
	var body model.Cashflow

	err := ctx.BodyParser(&body)
	if err != nil {
		return c.Status(501).JSON(fiber.Map{"status": "Failed", "message": "Periksa kembali inputan anda", "data": nil})
	}

	if body.Type == "debet" || body.Type == "kredit" {
		err := h.service.AddTransaction(ctx, &body)
		if err != nil {
			return ctx.Status(501).JSON(fiber.Map{"status": "Failed", "message": "Server sedang bermasalah, silahkan coba beberapa saat lagi", "data": nil})
		}
	} else {
		return c.Status(501).JSON(fiber.Map{"status": "Failed", "message": "Tipe transasksi salah", "data": nil})
	}

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{"status": "success", "message": "success", "data": cashflows,
		"total_debet":  total.Debet,
		"total_credit": total.Credit})
}

func (h Handler) CashflowEdit(ctx *fiber.Ctx) error {
	params := ctx.Params("cashflowId")
	paramsIdAccount := ctx.Params("accountId")

	var modelcashflow model.Cashflow
	ctx.BodyParser(&modelcashflow)

	//
	var modelaccount model.Account
	ctx.BodyParser(&modelaccount)

	data, err := h.cashflowService.EditCashflow(ctx, &modelcashflow, &modelaccount, params, paramsIdAccount)
	if err != nil {
		return ctx.Status(404).
			JSON(fiber.Map{"status": "Failed", "message": "Data not found", "data": nil})
	}
	return ctx.Status(201).
		JSON(fiber.Map{"status": "Success", "message": "User data retrieved", "data": data})
}

func (h Handler) CashflowDelete(ctx *fiber.Ctx) error {
	params := ctx.Params("cashflowId")
	paramsIdAccount := ctx.Params("accountId")

	err := h.cashflowService.DeleteCashflow(ctx, params, paramsIdAccount)
	if err != nil {
		return ctx.Status(404).
			JSON(fiber.Map{"status": "Failed", "message": "Data not found", "data": nil})
	}
	return ctx.Status(201).
		JSON(fiber.Map{"status": "Success", "message": "Cashflow deleted"})
}

}
